# Sorting App

Sorting App for learning fundamentals of JUnit and Maven.

## Quickstart

Testing:
```shell
mvn test
```

Packaging and running Uber-JAR (all dependencies and resources are packaged into single JAR):
```shell
mvn package
java -jar ./target/sorting-app-1.0-SNAPSHOT.jar -demo
java -jar ./target/sorting-app-1.0-SNAPSHOT.jar -array 1 3 -4
```

Generating Javadoc:
```shell
mvn javadoc:javadoc  # generates docs into ./target/site/apidocs/
```

## Notes
- Demo is based on loading [./src/main/resources/demo.json](./src/main/resources/demo.json) by using [com.google.code.gson](https://mvnrepository.com/artifact/com.google.code.gson/gson) dependency.
