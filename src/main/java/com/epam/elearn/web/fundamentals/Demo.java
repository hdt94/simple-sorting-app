package com.epam.elearn.web.fundamentals;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

/**
 * Demo of sorting by using {@link com.epam.elearn.web.fundamentals.App}.
 * Note: reading from resources is used for compliance of assignment only
 */
public class Demo {
    public String[][] values;

    public Demo() {
        values = new String[][]{{"2", "1"}};
    }

    public Demo(String[][] values) {
        this.values = values;
    }

    /**
     * Load default demo from JSON file
     */
    public static Demo loadDefaultResource() throws IOException {
        String filePath = "demo.json";
        ClassLoader loader = Demo.class.getClassLoader();

        try (InputStream input = loader.getResourceAsStream(filePath);
             InputStreamReader reader = new InputStreamReader(input, StandardCharsets.UTF_8);
        ) {
            Gson gson = new Gson();
            return gson.fromJson(new JsonReader(reader), Demo.class);
        } catch (IOException e) {
            return new Demo();
        }
    }
}
