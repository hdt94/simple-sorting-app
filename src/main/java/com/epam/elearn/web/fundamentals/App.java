package com.epam.elearn.web.fundamentals;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * App for sorting integer array and printing result
 */
public class App {

    /**
     * Demo sorting, or sort array and print result as list
     *
     * @param args -demo or -array followed by array of integer strings with max length of 10
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Use -demo or -array");
        }
        else if ((args.length == 1) && (args[0].equals("-demo"))) {
            try {
                demo();
            } catch (IOException e) {
                System.out.println("IOException: " + e.toString());
            }
        } else {
            String[] integers = Arrays.copyOfRange(args, 1, args.length);
            List<String> sortedIntegers = Arrays.asList(sortArray(integers));
            System.out.println(sortedIntegers);
        }
    }

    /**
     * Demo printing initial array and sorted array
     * It defaults to JSON file at src/main/resources/demo.json
     */
    public static void demo() throws IOException {
        Demo data = Demo.loadDefaultResource();
        for (String[] arr : data.values) {
            System.out.println("Initial:\t" + Arrays.asList(arr));
            System.out.println("Sorted:\t\t" + Arrays.asList(App.sortArray(arr)) + "\n");
        }
    }

    /**
     * Sort array
     *
     * @param input array of integer strings with max length of 10
     * @return new instance of sorted array
     */
    public static String[] sortArray(String[] input) throws IllegalArgumentException {
        if (input.length > 10) {
            throw new IllegalArgumentException("max args length is 10");
        } else if (input.length == 1) {
            // validate single element is integer
            int val = Integer.parseInt(input[0]);
        }

        return Arrays.stream(input)
                .sorted(Comparator.comparingInt(Integer::parseInt))
                .toArray(String[]::new);
    }

}
