package com.epam.elearn.web.fundamentals;

    import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AppExceptTest.class,
        AppParamTest.class,
        AppStdoutTest.class
})
public class AppSuiteTest {
}
