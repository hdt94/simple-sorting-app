package com.epam.elearn.web.fundamentals;

import org.junit.Test;

public class AppExceptTest {

    @Test(expected = IllegalArgumentException.class)
    public void IllegalLengthTest() {
        App.sortArray(new String[]{"", "", "", "", "", "", "", "", "", "", ""});
    }

    @Test(expected = java.lang.NumberFormatException.class)
    public void IncorrectFormatSingletonTest() {
        App.sortArray(new String[]{"a"});
    }

    @Test(expected = java.lang.NumberFormatException.class)
    public void IncorrectFormatTest() {
        App.sortArray(new String[]{"1", "a"});
    }
}
