package com.epam.elearn.web.fundamentals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


@RunWith(value = Parameterized.class)
public class AppParamTest {
    private final String[] expected;
    private final String[] input;

    public AppParamTest(String[] input, String[] expected) {
        this.expected = expected;
        this.input = input;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][][]{
                {new String[]{}, new String[]{}},
                {new String[]{"0"}, new String[]{"0"}},
                {new String[]{"01", "3", "2"}, new String[]{"01", "2", "3"}},
                {new String[]{"1", "-3", "2"}, new String[]{"-3", "1", "2"}},
                {new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}},
        });
    }


    @Test
    public void testSorting() {
        String[] actual = App.sortArray(this.input.clone());
        assertThat(actual, is(this.expected));
    }

}
