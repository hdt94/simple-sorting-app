package com.epam.elearn.web.fundamentals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class AppStdoutTest {
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final PrintStream stdout = System.out;

    @Before
    public void setupEach() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @After
    public void tearDownEach() {
        System.setOut(stdout);
    }

    @Test
    public void emptyStdoutTest() throws Exception {
        App.main(new String[]{"-array"});
        assertEquals("[]", outputStreamCaptor.toString().trim());
    }

    @Test
    public void singleElementStdoutTest() throws Exception {
        App.main(new String[]{"-array", "1"});
        assertEquals("[1]", outputStreamCaptor.toString().trim());
    }

    @Test
    public void multipleElementsStdoutTest() throws Exception {
        App.main(new String[]{"-array", "1", "2", "3"});
        assertEquals("[1, 2, 3]", outputStreamCaptor.toString().trim());
    }

}
